class SimpleCalculator
    def self.calculate(number1, number2, operator)
      if number1.class != String && number2.class != String && operator.class == String
        result = 0
        if operator == "+"
          result = number1 + number2
        elsif operator == "-"
          result = number1 - number2
        elsif operator == "*"
          result = number1 * number2
        elsif operator == "/"
          if number1 != 0 && number2 != 0
            result = number1 / number2
          else
            #puts("Division by zero is not allowed.")
            raise("Division by zero is not allowed")
          end
        else
          raise("UnsupportedOperation")
        end
        puts("#{number1} #{operator} #{number2} = #{result}")
      else
        raise("ArgumentError")
      end
    end
  end
