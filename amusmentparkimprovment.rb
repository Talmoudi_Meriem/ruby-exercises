class Attendee
    def initialize(height)
      @height = height
    end
    def issue_pass!(pass_id)
      @pass_id = pass_id
    end
    def revoke_pass!
      @pass_id = nil
    end
    
    def has_pass?
      @pass_id == nil ? false : true 
    end
    
    def fits_ride?(ride_minimum_height)
      if ride_minimum_height <= @height
        true 
        elsif ride_minimum_height > @height
        false
      end
    end
    def allowed_to_ride?(ride_minimum_height)
      if (ride_minimum_height <= @height && @pass_id != nil)
        true
      else false
      end
       
    end
  end
  
