class Diamond
    def initialize(letter)
      @letter = letter
    end
  
    def translate_letter
      @letter.downcase.ord - 96
    end
  
    def draw
      index = 1
      reverse = false
      r = ""
      while index != 0
        dots_nb = translate_letter - index
        for y in 1..dots_nb
          r += "."
        end
        r += (index + 96).chr.upcase
        if (index != 1)
          intra_dots = (translate_letter * 2 - 1) - ((dots_nb * 2) + 2)
          for y in 1..intra_dots
            r += "."
          end
          r += (index + 96).chr.upcase
        end
        for y in 1..dots_nb
          r += "."
        end
        r += "\n"
        if (reverse)
          index += -1
        elsif (index == translate_letter)
          reverse = true
          index += -1
        else
          index += 1
        end
      end
      puts r
    end
  end
  
  Diamond.new("Z").draw
