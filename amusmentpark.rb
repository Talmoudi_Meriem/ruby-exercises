class Attendee
    def initialize(h)
      @height = h
      @pass_id = nil
    end
  
    def height
      puts @height
    end
  
    def pass_id
      if (@pass_id != nil)
        @pass_id
      else
        "nil"
      end
    end
  
    def issue_pass!(newid)
      @pass_id = newid
    end
  
    def revoke_pass
      @pass_id = nil
    end
  
    def has_pass?
      puts @pass_id != nil
    end
  
    def fits_ride?(min_height)
      @height >= min_height
    end
  
    def allowed_to_ride?(min_height)
      puts pass_id != "nil" && fits_ride?(min_height)
    end
  end
  
  attendee = Attendee.new(100)
  attendee.issue_pass!(42)
  attendee.allowed_to_ride?(90)
  
