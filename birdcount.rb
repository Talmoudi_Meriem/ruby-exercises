class BirdCount
    def self.last_week
      puts "[0, 2, 5, 3, 7, 8, 4]"
    end
  
    def initialize(birds_per_day)
      @week = birds_per_day
    end
  
    def yesterday
      puts @week[-2]
    end
  
    def total
      puts @week.sum
    end
  
    def busy_days
      total = 0
      @week.each do |n|
        if (n >= 5)
          total += 1
        end
      end
      puts total
    end
  
    def day_without_birds?
      r = false
      @week.each do |n|
        r = n == 0
        break if r
      end
      puts r
    end
  end
